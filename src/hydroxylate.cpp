/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "math.h"
#include "stdlib.h"
#include "string.h"
#include "hydroxylate.h"
#include "atom.h"
#include "atom_vec.h"
#include "molecule.h"
#include "comm.h"
#include "irregular.h"
#include "modify.h"
#include "force.h"
#include "special.h"
#include "fix.h"
#include "compute.h"
#include "domain.h"
#include "lattice.h"
#include "region.h"
#include "input.h"
#include "variable.h"
#include "neighbor.h"
#include "random_park.h"
#include "random_mars.h"
#include "math_extra.h"
#include "math_const.h"
#include "error.h"

using namespace LAMMPS_NS;
using namespace MathConst;

#define BIG 1.0e30
#define EPSILON 1.0e-6

enum{BOX,REGION,SINGLE,RANDOM};
enum{ATOM,MOLECULE};
enum{LAYOUT_UNIFORM,LAYOUT_NONUNIFORM,LAYOUT_TILED};    // several files

/* ---------------------------------------------------------------------- */

Hydroxylate::Hydroxylate(LAMMPS *lmp) : Pointers(lmp) {}

/* ---------------------------------------------------------------------- */

void Hydroxylate::command(int narg, char **arg)
{

  x = atom->x;

  atype = force->inumeric(FLERR,arg[0]); 		// Establish type of atom we're adding as 
  												//	argument 1

  btype = force->inumeric(FLERR,arg[1]); 		// Establish what type of atom we're 
  												// attaching to as argument 2
  
  ctype = force->inumeric(FLERR,arg[2]);		// Establish atom we're counting bonds to
  												// as argument 3

  maxbonds = force->numeric(FLERR,arg[3]);		// Establish when to form bonds (i.e. if
  												// total number of bonds is less than
  												// maxbonds) as argument 4
  
  bondlength = force->numeric(FLERR, arg[4]);	// How long to make the bond as argument 5
  
  bondangle = force->numeric(FLERR, arg[5]);	// Angle of bond as argument 6
  
  bondtype = force->numeric(FLERR, arg[6]);		// Establish which type of bond to make
  												// as argument 7

  nregion = domain->find_region(arg[7]);		// Which region are we in?; Argument 8
  
  btype2 = force->inumeric(FLERR,arg[8]);		// What type of atom we're changing btype to
  
  ctype2 = force->inumeric(FLERR,arg[9]);		// What type of atom we're changing ctype to

  domain->regions[nregion]->init();
  domain->regions[nregion]->prematch();

  // process optional keywords

  int scaleflag = 1;
  remapflag = 0;
  mode = ATOM;
  int molseed;
  varflag = 0;
  vstr = xstr = ystr = zstr = NULL;
  quatone[0] = quatone[1] = quatone[2] = 0.0;

  nbasis = domain->lattice->nbasis;
  basistype = new int[nbasis];
  for (int i = 0; i < nbasis; i++) basistype[i] = atype;


  // error checks

  if (mode == ATOM && (atype <= 0 || atype > atom->ntypes))
    error->all(FLERR,"Invalid atom type in create_atoms command");

  // error check and further setup for mode = MOLECULE

  // error check and further setup for variable test
  // save local copy of each equal variable string so can restore at end


  // demand non-none lattice be defined for BOX and REGION
  // else setup scaling for SINGLE and RANDOM
  // could use domain->lattice->lattice2box() to do conversion of
  //   lattice to box, but not consistent with other uses of units=lattice
  // triclinic remapping occurs in add_single()

  if (style == BOX || style == REGION) {
    if (nbasis == 0)
      error->all(FLERR,"Cannot create atoms with undefined lattice");
  } else if (scaleflag == 1) {
    xone[0] *= domain->lattice->xlattice;
    xone[1] *= domain->lattice->ylattice;
    xone[2] *= domain->lattice->zlattice;
  }

  // set bounds for my proc in sublo[3] & subhi[3]
  // if periodic and style = BOX or REGION, i.e. using lattice:
  //   should create exactly 1 atom when 2 images are both "on" the boundary
  //   either image may be slightly inside/outside true box due to round-off
  //   if I am lo proc, decrement lower bound by EPSILON
  //     this will insure lo image is created
  //   if I am hi proc, decrement upper bound by 2.0*EPSILON
  //     this will insure hi image is not created
  //   thus insertion box is EPSILON smaller than true box
  //     and is shifted away from true boundary
  //     which is where atoms are likely to be generated

  triclinic = domain->triclinic;

  double epsilon[3];
  if (triclinic) epsilon[0] = epsilon[1] = epsilon[2] = EPSILON;
  else {
    epsilon[0] = domain->prd[0] * EPSILON;
    epsilon[1] = domain->prd[1] * EPSILON;
    epsilon[2] = domain->prd[2] * EPSILON;
  }

  if (triclinic == 0) {
    sublo[0] = domain->sublo[0]; subhi[0] = domain->subhi[0];
    sublo[1] = domain->sublo[1]; subhi[1] = domain->subhi[1];
    sublo[2] = domain->sublo[2]; subhi[2] = domain->subhi[2];
  } else {
    sublo[0] = domain->sublo_lamda[0]; subhi[0] = domain->subhi_lamda[0];
    sublo[1] = domain->sublo_lamda[1]; subhi[1] = domain->subhi_lamda[1];
    sublo[2] = domain->sublo_lamda[2]; subhi[2] = domain->subhi_lamda[2];
  }

  if (style == BOX || style == REGION) {
    if (comm->layout != LAYOUT_TILED) {
      if (domain->xperiodic) {
        if (comm->myloc[0] == 0) sublo[0] -= epsilon[0];
        if (comm->myloc[0] == comm->procgrid[0]-1) subhi[0] -= 2.0*epsilon[0];
      }
      if (domain->yperiodic) {
        if (comm->myloc[1] == 0) sublo[1] -= epsilon[1];
        if (comm->myloc[1] == comm->procgrid[1]-1) subhi[1] -= 2.0*epsilon[1];
      }
      if (domain->zperiodic) {
        if (comm->myloc[2] == 0) sublo[2] -= epsilon[2];
        if (comm->myloc[2] == comm->procgrid[2]-1) subhi[2] -= 2.0*epsilon[2];
      }
    } else {
      if (domain->xperiodic) {
        if (comm->mysplit[0][0] == 0.0) sublo[0] -= epsilon[0];
        if (comm->mysplit[0][1] == 1.0) subhi[0] -= 2.0*epsilon[0];
      }
      if (domain->yperiodic) {
        if (comm->mysplit[1][0] == 0.0) sublo[1] -= epsilon[1];
        if (comm->mysplit[1][1] == 1.0) subhi[1] -= 2.0*epsilon[1];
      }
      if (domain->zperiodic) {
        if (comm->mysplit[2][0] == 0.0) sublo[2] -= epsilon[2];
        if (comm->mysplit[2][1] == 1.0) subhi[2] -= 2.0*epsilon[2];
      }
    }
  }
  
  bigint natoms_previous = atom->natoms;
  int nlocal_previous = atom->nlocal;
  add_atoms();

  // invoke set_arrays() for fixes/computes/variables
  //   that need initialization of attributes of new atoms
  // don't use modify->create_attributes() since would be inefficient
  //   for large number of atoms
  // note that for typical early use of create_atoms,
  //   no fixes/computes/variables exist yet

  int nlocal = atom->nlocal;
  for (int m = 0; m < modify->nfix; m++) {
    Fix *fix = modify->fix[m];
    if (fix->create_attribute)
      for (int i = nlocal_previous; i < nlocal; i++)
        fix->set_arrays(i);
  }
  for (int m = 0; m < modify->ncompute; m++) {
    Compute *compute = modify->compute[m];
    if (compute->create_attribute)
      for (int i = nlocal_previous; i < nlocal; i++)
        compute->set_arrays(i);
  }
  for (int i = nlocal_previous; i < nlocal; i++)
    input->variable->set_arrays(i);

  // restore each equal variable string previously saved

  if (varflag) {
    if (xstr) input->variable->equal_restore(xvar,xstr_copy);
    if (ystr) input->variable->equal_restore(yvar,ystr_copy);
    if (zstr) input->variable->equal_restore(zvar,zstr_copy);
  }

  // set new total # of atoms and error check

  bigint nblocal = atom->nlocal;
  MPI_Allreduce(&nblocal,&atom->natoms,1,MPI_LMP_BIGINT,MPI_SUM,world);
  if (atom->natoms < 0 || atom->natoms >= MAXBIGINT)
    error->all(FLERR,"Too many total atoms");

  // add IDs for newly created atoms
  // check that atom IDs are valid

  if (atom->tag_enable) atom->tag_extend();
  atom->tag_check();

  // create global mapping of atoms
  // zero nghost in case are adding new atoms to existing atoms

  if (atom->map_style) {
    atom->nghost = 0;
    atom->map_init();
    atom->map_set();
  }

  // for MOLECULE mode:
  // set molecule IDs for created atoms if used
  // reset new molecule bond,angle,etc and special values
  // send atoms to new owning procs via irregular comm
  //   since not all atoms I created will be within my sub-domain
  // perform special list build if needed

  if (mode == MOLECULE) {

    // molcreate = # of molecules I created

    int molcreate = (atom->nlocal - nlocal_previous) / onemol->natoms;

    // increment total bonds,angles,etc 
    
    bigint nmolme = molcreate;
    bigint nmoltotal;
    MPI_Allreduce(&nmolme,&nmoltotal,1,MPI_LMP_BIGINT,MPI_SUM,world);
    atom->nbonds += nmoltotal * onemol->nbonds;
    atom->nangles += nmoltotal * onemol->nangles;
    atom->ndihedrals += nmoltotal * onemol->ndihedrals;
    atom->nimpropers += nmoltotal * onemol->nimpropers;

    // if atom style template
    // maxmol = max molecule ID across all procs, for previous atoms
    // moloffset = max molecule ID for all molecules owned by previous procs
    //             including molecules existing before this creation

    tagint moloffset;
    tagint *molecule = atom->molecule;
    if (molecule) {
      tagint max = 0;
      for (int i = 0; i < nlocal_previous; i++) max = MAX(max,molecule[i]);
      tagint maxmol;
      MPI_Allreduce(&max,&maxmol,1,MPI_LMP_TAGINT,MPI_MAX,world);
      MPI_Scan(&molcreate,&moloffset,1,MPI_INT,MPI_SUM,world);
      moloffset = moloffset - molcreate + maxmol;
    }

    // loop over molecules I created
    // set their molecule ID
    // reset their bond,angle,etc and special values

    int natoms = onemol->natoms;
    tagint offset = 0;

    tagint *tag = atom->tag;
    int *num_bond = atom->num_bond;
    int *num_angle = atom->num_angle;
    int *num_dihedral = atom->num_dihedral;
    int *num_improper = atom->num_improper;
    tagint **bond_atom = atom->bond_atom;
    tagint **angle_atom1 = atom->angle_atom1;
    tagint **angle_atom2 = atom->angle_atom2;
    tagint **angle_atom3 = atom->angle_atom3;
    tagint **dihedral_atom1 = atom->dihedral_atom1;
    tagint **dihedral_atom2 = atom->dihedral_atom2;
    tagint **dihedral_atom3 = atom->dihedral_atom3;
    tagint **dihedral_atom4 = atom->dihedral_atom4;
    tagint **improper_atom1 = atom->improper_atom1;
    tagint **improper_atom2 = atom->improper_atom2;
    tagint **improper_atom3 = atom->improper_atom3;
    tagint **improper_atom4 = atom->improper_atom4;
    int **nspecial = atom->nspecial;
    tagint **special = atom->special;
    int molecular = atom->molecular;

    int ilocal = nlocal_previous;
    for (int i = 0; i < molcreate; i++) {
      if (tag) offset = tag[ilocal]-1;
      for (int m = 0; m < natoms; m++) {
        if (molecular) molecule[ilocal] = moloffset + i+1;
        if (molecular == 2) {
          atom->molindex[ilocal] = 0;
          atom->molatom[ilocal] = m;
        } else if (molecular) {
          if (onemol->bondflag)
            for (int j = 0; j < num_bond[ilocal]; j++)
              bond_atom[ilocal][j] += offset;
          if (onemol->angleflag)
            for (int j = 0; j < num_angle[ilocal]; j++) {
              angle_atom1[ilocal][j] += offset;
              angle_atom2[ilocal][j] += offset;
              angle_atom3[ilocal][j] += offset;
            }
          if (onemol->dihedralflag)
            for (int j = 0; j < num_dihedral[ilocal]; j++) {
              dihedral_atom1[ilocal][j] += offset;
              dihedral_atom2[ilocal][j] += offset;
              dihedral_atom3[ilocal][j] += offset;
              dihedral_atom4[ilocal][j] += offset;
            }
          if (onemol->improperflag)
            for (int j = 0; j < num_improper[ilocal]; j++) {
              improper_atom1[ilocal][j] += offset;
              improper_atom2[ilocal][j] += offset;
              improper_atom3[ilocal][j] += offset;
              improper_atom4[ilocal][j] += offset;
            }
          if (onemol->specialflag)
            for (int j = 0; j < nspecial[ilocal][2]; j++) 
              special[ilocal][j] += offset;
        }
        ilocal++;
      }
    }

    // perform irregular comm to migrate atoms to new owning procs

    imageint *image = atom->image;
    int nlocal = atom->nlocal;
    for (int i = 0; i < nlocal; i++) domain->remap(x[i],image[i]);

    if (domain->triclinic) domain->x2lamda(atom->nlocal);
    domain->reset_box();
    Irregular *irregular = new Irregular(lmp);
    irregular->migrate_atoms(1);
    delete irregular;
    if (domain->triclinic) domain->lamda2x(atom->nlocal);
  }

  // clean up

  if (domain->lattice) delete [] basistype;
  delete [] vstr;
  delete [] xstr;
  delete [] ystr;
  delete [] zstr;

  // print status

  if (comm->me == 0) {
    if (screen)
      fprintf(screen,"Created " BIGINT_FORMAT " atoms\n",
              atom->natoms-natoms_previous);
    if (logfile)
      fprintf(logfile,"Created " BIGINT_FORMAT " atoms\n",
              atom->natoms-natoms_previous);
  }
}

/* ----------------------------------------------------------------------
   add atoms in between other atoms if they're below the threshold length
------------------------------------------------------------------------- */

void Hydroxylate::add_atoms()
{
  // remap atom if requested
  if (remapflag) {
    imageint imagetmp = ((imageint) IMGMAX << IMG2BITS) |
      ((imageint) IMGMAX << IMGBITS) | IMGMAX;
    domain->remap(xone,imagetmp);
  }
  
  // if triclinic, convert to lamda coords (0-1)
  int totalatoms = atom->nlocal + atom->nghost;
  int *type = atom->type;
	double tempatoms[totalatoms][3];
	int atoms = 0, firstbond = 0;
	double vec1[3], vec2[3];
	 
  for (int i = 0; i < atom->nlocal; i++){ // Rewrite code so that type[i] can be atype or btype
  	bonds = 0;
  	if (type[i] != btype) continue;
  	if (!(domain->regions[nregion]->match(x[i][0],x[i][1],x[i][2]))) continue;
  	for (int j = 0; j < atom->num_bond[i]; j++){
  		if (type[atom->bond_atom[i][j]] == ctype) {
  			bonds++;
  			firstbond = atom->map(atom->bond_atom[i][j]);
  			}
  	}
  	if ((bonds > 0)&&(bonds<maxbonds)){
  		type[i] = btype2;
  		type[firstbond] = ctype2; // We need to fix this, too. We need to change the ctypes to ctype2 at the end of the process
		unitvector(i, firstbond, vec1);
		anglevector(vec1, vec2, bondangle);
		multiplyvector(bondlength, vec2, 3);
		addvector(x[i], vec2, 3);
  		atoms++;
  		for (int j = 0; j < 3; j++) tempatoms[atoms][j] = vec2[j];
  		}
  	firstbond = 0;
	}
  for (int i = 0; i < atoms; i++) atom->avec->create_atom(atype, tempatoms[i]);
  printf("\nThere are now %i new atoms from hydroxylate.\n", atoms);
}


/* ----------------------------------------------------------------------
   test a generated atom position against variable evaluation
   first plug in x,y,z values as requested
------------------------------------------------------------------------- */

int Hydroxylate::vartest(double *x)
{
  if (xstr) input->variable->equal_override(xvar,x[0]);
  if (ystr) input->variable->equal_override(yvar,x[1]);
  if (zstr) input->variable->equal_override(zvar,x[2]);

  double value = input->variable->compute_equal(vvar);
  if (value == 0.0) return 0;
  return 1;
}

double Hydroxylate::distance(int atom1, int atom2){
	double square = 0;
	for (int i = 0; i < 3; i++) square += pow(x[atom1][i] - x[atom2][i], 2);
	return sqrt(square);
}

void Hydroxylate::midpoint(int atom1, int atom2, double *coords){
	for (int i = 0; i < 3; i++) {
		coords[i] = (x[atom1][i] + x[atom2][i])/2.0;
	}
}

void Hydroxylate::unitvector(int atom1, int atom2, double *vector){
	double normalize = 0;
	//printf("%f %i %i\n", normalize, atom1, atom2);
	if (distance(atom1, atom2) > 0) normalize = 1.0/distance(atom1, atom2);
	//printf("%f\n", normalize);
	for (int i = 0; i < 3; i++) vector[i] = normalize*(x[atom2][i] - x[atom1][i]);
	//printvector(vector, 3);
}

void Hydroxylate::unitvector(double *vector, int n){
	double normalize = 0;
	normalize = 1.0/dotproduct(vector, vector, n);
	for (int i = 0; i < n; i++) vector[i] = normalize*vector[i];
}

void Hydroxylate::anglevector(double *vector1, double *vector2, double angle){	
	angle = M_PI/180*angle;
	double cosine = cos(angle);
	double sine = sin(angle);
	
	double tempvector[3];
	
	for (int i = 0; i < 3; i++) {
		tempvector[i] = 0;
		vector2[i] = vector1[i];
	}
	bool working = 1;
	while (working){	
		for (int i = 0; i < 2; i++){
			tempvector[i] = (double) (rand() % 5000)/10000.0 - 1;
			}
		if (dotproduct(tempvector, tempvector, 3) < 1) working = 0;
		}
	
	tempvector[2] = sqrt(1 - dotproduct(tempvector, tempvector, 3));
	
	multiplyvector(dotproduct(tempvector, vector1, 3), vector2, 3);
	
	subtractvector(vector2, tempvector, 3);
	
	unitvector(tempvector, 3);
	
	//printvector(tempvector, 3);
	
	double ux = tempvector[0], uy = tempvector[1], uz = tempvector[2];
	double x = vector1[0], y = vector1[1], z = vector1[2];
	
	vector2[0] = x*(cosine + pow(ux, 2)*(1 - cosine)) + y*(ux*uy*(1 - cosine) - uz*sine)
		+ z*(ux*uz*(1 - cosine) + uy*sine);
	vector2[1] = x*(uy*ux*(1 - cosine) + uz*sine) + y*(cosine + pow(uy, 2)*(1 - cosine))
		+ z*(uy*uz*(1 - cosine) - ux*sine);
	vector2[2] = x*(uz*ux*(1 - cosine) - uy*sine) + y*(uz*uy*(1 - cosine) + ux*sine)
		+ z*(cosine + pow(uz, 2)*(1 - cosine));
		
	//printf("%f\n", acos(dotproduct(vector1, vector2, 3))*180/M_PI);

}

void Hydroxylate::printvector(double *vector, int n){
	printf("(");
	for (int i = 0; i < n; i++) printf("%f, ", vector[i]);
	printf("\b\b)\n");
}

void Hydroxylate::addvector(double *vector1, double *vector2, int n){
	for (int i = 0; i < n; i++) vector2[i] = vector1[i] + vector2[i];
}

void Hydroxylate::subtractvector(double *vector1, double *vector2, int n){
	for (int i = 0; i < n; i++) vector2[i] = vector1[i] - vector2[i];
}

void Hydroxylate::multiplyvector(double a, double *vector, int n){
	for (int i = 0; i < n; i++) vector[i] = a*vector[i];
}

double Hydroxylate::dotproduct(double *vector1, double *vector2, int n){
	double sum = 0;
	for (int j = 0; j < n; j++) sum = sum + vector1[j]*vector2[j];
	return sum;
}